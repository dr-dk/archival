import typer

from rich import print as rprint

from drdk_api import Series

from .util import format_series_list, load_list, save_list

app = typer.Typer()


def select_series(series_list: list[Series]) -> list[Series]:
    def select_from_index(index: int):
        try:
            return series_list[index]
        except IndexError:
            print("Your index doesn't correnspond to a series in the table above")
            raise typer.Abort()

    rprint(format_series_list(series_list))

    try:
        series_indices: list[int] = [
            int(i)
            for i in typer.prompt(
                "Please enter the index of the series you would like to select as either single number or comma-seperated list"
            ).split(",")
        ]
    except ValueError:
        print(
            "It appears that you have entered something that wasn't a number or comma"
        )
        raise typer.Abort()

    return [select_from_index(index) for index in series_indices]


@app.command()
def search_series(search_query: str) -> None:
    rprint(Series.search_series(search_query))


@app.command()
def add(search_query: str) -> None:
    series_list: list[Series] = Series.search_series(search_query)
    selected_series: list[Series] = select_series(series_list)

    stored_series: list[Series | None] = load_list()

    for series in selected_series:
        if series not in stored_series:
            stored_series.append(series)
            print("Save new series to list with title:", series.title)
        else:
            print("Series is already in list. Title:", series.title)

    save_list(stored_series)


@app.command()
def remove() -> None:
    series_list: list[Series | None] = load_list()

    if not series_list:
        print("Config file is currently empty")
        raise typer.Abort()

    selected_series: list[Series] = select_series(series_list)

    for series in selected_series:
        series_list.remove(series)

    save_list(series_list)
    print("Successfully removed element(s) from list")
