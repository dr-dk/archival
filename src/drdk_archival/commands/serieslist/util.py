from typer import Abort
from rich.table import Table

import dataclasses
import os
import json

from drdk_api import Series


def format_series_list(series_list: list[Series]) -> Table:
    table = Table(show_header=True, show_lines=True, header_style="bold magenta")

    table.add_column("Nr.", style="dim")
    table.add_column("Title", justify="center")
    table.add_column("URN", style="dim")

    for i, series in enumerate(series_list):
        table.add_row(str(i), series.title, series.urn)

    return table


def load_list() -> list[Series | None]:
    if not os.path.isfile("list.json"):
        return []

    with open("list.json", "r") as f:
        try:
            series_list: list[dict[str, str]] = json.load(f)
        except json.JSONDecodeError:
            print("There's some kind of problem with your list file")
            raise Abort()

        if series_list:
            return Series.from_json(series_list)
        else:
            return []


def save_list(series_list: list[Series]) -> None:
    deserialized_series: list[dict[str, str]] = [
        dataclasses.asdict(series) for series in series_list
    ]

    with open("list.json", "w") as f:
        json.dump(deserialized_series, f)
