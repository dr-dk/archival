import typer

from .commands.serieslist.app import app as listApp

app = typer.Typer()
app.add_typer(listApp, name="list")

if __name__ == "__main__":
    app()
